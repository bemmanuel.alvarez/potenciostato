%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       Instituto de Ingeniería UNAM    %%%
%%%                  BEAS                  %%%
%%%                  RGRCH                 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
clc
close all

archivo = 'test';
col_TIA = 'C2:C53';
col_GEN = 'B2:B53';
Rf = 1e3;

volt_GEN = xlsread(archivo, col_GEN);
volt_TIA = xlsread(archivo, col_TIA);
corriente_TIA = volt_TIA/Rf;
corriente_TIA = corriente_TIA*1000;

x = max(volt_GEN) - min(volt_GEN);
x = x/10;
y = max(corriente_TIA) - min(corriente_TIA);
y = y/10;

plot(volt_GEN, corriente_TIA,'r')
xlim([min(volt_GEN)-x max(volt_GEN)+x]);
ylim([min(corriente_TIA)-y max(corriente_TIA)+y]);
xlabel('Voltaje (V)');
ylabel('Corriente (mA)');